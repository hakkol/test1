<?php
class uriAnalyze {

	private function getTags($uri) {
		$arrayTags = explode("/", $uri);
		return $arrayTags;
	}

	function getElementsArray($uri) {
		$ElementsUri = $this->getTags($uri);
		if (!is_numeric($ElementsUri[0]) && is_numeric($ElementsUri[1]) && is_numeric($ElementsUri[2])) {
				$arResult['tag'] = $ElementsUri[0];
				$arResult['width'] = $ElementsUri[1];
				$arResult['height'] = $ElementsUri[2];
				return $arResult;
				
		}elseif(is_numeric($ElementsUri[0]) && is_numeric($ElementsUri[1])) {
				$arResult['width'] = $ElementsUri[0];
				$arResult['height'] = $ElementsUri[1];
				return $arResult;
		
		}else{
			return false;
		}
	}
}