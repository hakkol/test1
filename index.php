<?php 
require_once 'createimage.php';
//require_once 'uri_analyze.php';
require_once 'vendor/autoload.php';

$uriAnalyzer = new uriAnalyze;
$ElementsUri = $uriAnalyzer->getElementsArray($_GET['URI']);

if($ElementsUri != false){
	if(isset($ElementsUri['tag'])){
		$imageObj = new Image;
		$imageId = $imageObj->findImageForTag($ElementsUri['tag']);
		if ($imageId != false){
			$imageId = $imageObj->optimalResize($imageId, $ElementsUri['width'], $ElementsUri['height']);
		}else{
			$imageId = $imageObj->create($ElementsUri['width'], $ElementsUri['height']);	
		}
		header('Content-Type: image/png');
		imagepng($imageId);
		imagedestroy($imageId);
	}else{
		$imageObj = new Image;
		$imageId = $imageObj->create($ElementsUri['width'], $ElementsUri['height']);	
		header('Content-Type: image/png');
		imagepng($imageId);
		imagedestroy($imageId);
	}
}else{
		header("HTTP/1.1 404 Not Found");
}