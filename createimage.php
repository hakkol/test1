<?php
	class Image {
		
		function create($width, $height){
			$font_file = 'font/Tahoma.ttf';
			$im = imagecreate($width, $height);
			$bgt = ImageColorAllocate ($im, 135, 130,130);
			$textcolor = imagecolorallocate($im, 89, 81, 81);
			imagefilledrectangle($im, 0, 0, 150, 30, $bgt);
			$lenth=9999;
			for ($delta=0;$lenth>$width/2; $delta++){				
				$bbox = imageftbbox(($height/2-$delta), 0, $font_file, "$width x $height");
				$lenth = $bbox[2]-$bbox[0];
			}
			$font_size = $height/2-$delta;			
			imagefttext($im, $font_size, 0, ($width-$lenth)/2, ($height+$font_size)/2, $textcolor, $font_file, "$width x $height");
			return $im;
		}
		function findImageForTag($tag){
			$path = "img/".$tag.".png";
			if (file_exists($path)){
				$im = imagecreatefrompng($path);
				return $im;
			}else{
				return false;
			}
			
		}		
		function optimalResize ($imgOriginal, $newWidth, $newHeight){
		
			$imageResized = imagecreatetruecolor($newWidth , $newHeight);
			$originalWidth  = imagesx($imgOriginal);  
			$originalHeight = imagesy($imgOriginal); 
			$newRatio = $newWidth/$newHeight;
			$originalRatio = $originalWidth/$originalHeight;
			
			if ($originalRatio < $newRatio){
				$heghtCenter = $originalHeight/2;
				$newRatio = $newWidth/$newHeight;
				$croppedHeight = $originalWidth/$newRatio;
				$heightShift = ($originalHeight-$croppedHeight)/2;
				$croppedWidth = $originalWidth;
				$widthShift = 0;
			} else {
				$widthCenter = $originalWidth/2;
				$newRatio = $newHeight/$newWidth;
				$croppedWidth = $originalWidth/$newRatio;
				$widthShift = ($originalWidth-$croppedWidth)/2;
				$croppedHeight = $originalHeight;
				$heightShift = 0;			
			}

			imagecopyresampled($imageResized, $imgOriginal, 0, 0, $widthShift, $heightShift, $newWidth, $newHeight , $croppedWidth, $croppedHeight);
			return $imageResized;
		}
	}

